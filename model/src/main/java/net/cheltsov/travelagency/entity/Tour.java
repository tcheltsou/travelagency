package net.cheltsov.travelagency.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@javax.persistence.Entity
@Data
@Table(name = "tour")
@AttributeOverride(name = "id", column = @Column(name = "tour_id"))
public class Tour extends Entity {
    private String photo;
    @Column(columnDefinition = "timestamp")
    private LocalDate date;
    private Integer duration;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column(name = "tour_type_id")
    private TourType type;
    private String description;
    private BigDecimal cost;


}
