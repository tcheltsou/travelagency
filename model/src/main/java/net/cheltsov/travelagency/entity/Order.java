package net.cheltsov.travelagency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Objects;

@javax.persistence.Entity
@Data
@Table(name = "\"order\"")
@AttributeOverride(name = "id", column = @Column(name = "order_id"))
public class Order extends Entity {
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tour_id")
    private Tour tour;
    private Integer quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        if (!super.equals(o)) return false;
        Order order = (Order) o;
        return Objects.equals(user.getId(), order.user.getId()) &&
                Objects.equals(tour, order.tour) &&
                Objects.equals(quantity, order.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), user.getId(), tour, quantity);
    }


    @Override
    public String toString() {
        return "Order{" +
                "user_id=" + user.getId() +
                ", tour_id=" + tour.getId() +
                ", quantity=" + quantity +
                '}';
    }
}
