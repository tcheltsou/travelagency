package net.cheltsov.travelagency.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@javax.persistence.Entity
@Data
@Table(name = "review")
@AttributeOverride(name = "id", column = @Column(name = "review_id"))
public class Review extends Entity {
    @ManyToOne
    @JoinColumn(name = "tour_id")
    private Tour tour;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    private String content;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;
        if (!super.equals(o)) return false;
        Review review = (Review) o;
        return Objects.equals(tour, review.tour) &&
                Objects.equals(user.getId(), review.user.getId()) &&
                Objects.equals(content, review.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tour, user.getId(), content);
    }


    @Override
    public String toString() {
        return "Review{" +
                "tour=" + tour +
                ", user_id=" + user.getId() +
                ", content='" + content + '\'' +
                '}';
    }
}
