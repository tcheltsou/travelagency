package net.cheltsov.travelagency.entity;


public enum TourType {
    OUTDOOR,
    FAMILY,
    HOLIDAY,
    CYCLE,
    CULINARY,
    LUXURY
}
