package net.cheltsov.travelagency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Table;


@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "country_id"))
@Table(name = "country")
@Data @NoArgsConstructor @EqualsAndHashCode(callSuper = true)
public class Country extends Entity {
    private String name;
    public Country(String name) {
        this.name = name;
    }
}
