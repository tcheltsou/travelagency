package net.cheltsov.travelagency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "hotel_id"))
@Table(name = "hotel")
@Data @EqualsAndHashCode(callSuper = true)
public class Hotel extends Entity {
    private String name;
    @Column(columnDefinition = "char")
    private String phone;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
    private Integer stars;
}
