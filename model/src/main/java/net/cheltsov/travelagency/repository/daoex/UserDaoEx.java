package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Order;
import net.cheltsov.travelagency.entity.Review;
import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("daoExs")
@Repository("userDao")
public class UserDaoEx extends AbstractDaoEx<User> {
    private static final String SQL_FIND_ALL = "SELECT user_id, password, login FROM \"user\"";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE user_id = :user_id";
    private static final String SQL_FIND_ORDERS = OrderDaoEx.SQL_FIND_ALL + " WHERE u.user_id = :user_id";
    private static final String SQL_FIND_REVIEWS = ReviewDaoEx.SQL_FIND_ALL + " WHERE u.user_id = :user_id";

    public static final UserRowMapper USER_ROW_MAPPER = new UserRowMapper();

    @Override
    public User create(User user) throws RepositoryException {
        return create(user, getEntityParameters(user));
    }

    @Transactional(readOnly = true, isolation = Isolation.SERIALIZABLE)
    @Override
    public User read(int id) throws RepositoryException {
        return setLists(jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("user_id", id), USER_ROW_MAPPER));
    }

    private User setLists(User user) {
        List<Order> orders = jdbcTemplate.query(SQL_FIND_ORDERS,
                new MapSqlParameterSource("user_id", user.getId()), OrderDaoEx.ORDER_ROW_MAPPER);
        List<Review> reviews = jdbcTemplate.query(SQL_FIND_REVIEWS,
                new MapSqlParameterSource("user_id", user.getId()), ReviewDaoEx.REVIEW_ROW_MAPPER);
        orders.forEach(order -> order.setUser(user));
        reviews.forEach(review -> review.setUser(user));
        user.setOrders(orders);
        user.setReviews(reviews);
        return user;
    }

    @Transactional(readOnly = true, isolation = Isolation.SERIALIZABLE)
    @Override
    public List<User> readAll() throws RepositoryException {
        List<User> users = jdbcTemplate.query(SQL_FIND_ALL, USER_ROW_MAPPER);
        users.forEach(this::setLists);
        return users;
    }

    @Override
    public boolean update(User user) throws RepositoryException {
        return update(user, getEntityParameters(user));
    }

    private Map<String, Object> getEntityParameters(User user) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("password", String.valueOf(user.getPassword()));
        parameters.put("login", user.getLogin());
        return parameters;
    }

    public static class UserRowMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getInt("user_id"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password").toCharArray());
            return user;
        }
    }
}
