package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Country;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("countryDao")
public class CountryJpa extends AbstractJpa<Country> {
    public CountryJpa() {
        super(Country.class);
    }
}
