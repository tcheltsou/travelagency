package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Hotel;
import net.cheltsov.travelagency.entity.Tour;
import net.cheltsov.travelagency.entity.TourType;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("daoExs")
@Repository("tourDao")
public class TourDaoEx extends AbstractDaoEx<Tour> {
    private static final String SQL_FIND_ALL = "SELECT t.tour_id, t.date, t.duration, t.tour_type_id, "
            + " t.description, t.cost, t.photo, "
            + " t.hotel_id, h.name AS hotelName, h.phone, h.stars, "
            + " h.country_id, c.name AS countryName "
            + " FROM tour t "
            + " JOIN (hotel h "
            + "     JOIN country c ON h.country_id = c.country_id)"
            + " ON t.hotel_id = h.hotel_id";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE t.tour_id = :tour_id";
    public static final TourRowMapper TOUR_ROW_MAPPER = new TourRowMapper();

    @Override
    public Tour create(Tour tour) throws RepositoryException {
        return create(tour, getEntityParameters(tour));
    }

    @Override
    public Tour read(int id) throws RepositoryException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("tour_id", id),
                TOUR_ROW_MAPPER);
    }

    @Override
    public List<Tour> readAll() throws RepositoryException {
        return jdbcTemplate.query(SQL_FIND_ALL, TOUR_ROW_MAPPER);
    }

    @Override
    public boolean update(Tour tour) throws RepositoryException {
        return update(tour, getEntityParameters(tour));
    }

    private Map<String , Object> getEntityParameters(Tour tour) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("date", tour.getDate());
        parameters.put("duration", tour.getDuration());
        parameters.put("country_id", tour.getCountry() != null? tour.getCountry().getId(): 0);
        parameters.put("tour_type_id", tour.getType() != null? tour.getType().ordinal(): 0);
        parameters.put("description", tour.getDescription());
        parameters.put("cost", tour.getCost());
        parameters.put("photo", tour.getPhoto());
        parameters.put("hotel_id", tour.getHotel() != null? tour.getHotel().getId(): 0);
        return parameters;
    }

    public static class TourRowMapper implements RowMapper<Tour> {
        @Override
        public Tour mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hotel hotel = HotelDaoEx.HOTEL_ROW_MAPPER.mapRow(rs, rowNum);
            Tour tour = new Tour();
            tour.setHotel(hotel);
            tour.setId(rs.getInt("tour_id"));
            tour.setDescription(rs.getString("description"));
            tour.setPhoto(rs.getString("photo"));
            tour.setType(TourType.values()[rs.getInt("tour_type_id")]);
            tour.setDate(rs.getTimestamp("date").toLocalDateTime().toLocalDate());
            tour.setCost(rs.getBigDecimal("cost"));
            tour.setDuration(rs.getInt("duration"));
            tour.setCountry(hotel.getCountry());
            return tour;
        }
    }
}
