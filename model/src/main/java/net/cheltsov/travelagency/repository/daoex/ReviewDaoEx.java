package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Review;
import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("daoExs")
@Repository("reviewDao")
public class ReviewDaoEx extends AbstractDaoEx<Review> {
    static final String SQL_FIND_ALL = "SELECT r.review_id, r.content, "
            + " r.user_id, u.password, u.login, "
            + " t.tour_id, t.date, t.duration, t.tour_type_id, "
            + " t.description, t.cost, t.photo, "
            + " t.hotel_id, h.name AS hotelName, h.phone, h.stars, "
            + " h.country_id, c.name AS countryName "
            + " FROM review r "
            + " JOIN \"user\" u ON r.user_id = u.user_id "
            + " JOIN (tour t "
            + "     JOIN (hotel h "
            + "         JOIN country c ON h.country_id = c.country_id) "
            + "     ON t.hotel_id = h.hotel_id)"
            + " ON r.tour_id = t.tour_id";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE r.review_id = :review_id";
    static final ReviewRowMapper REVIEW_ROW_MAPPER = new ReviewRowMapper();

    @Override
    public Review create(Review review) throws RepositoryException {
        return create(review, getEntityParameters(review));
    }

    @Override
    public Review read(int id) throws RepositoryException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("review_id", id),
                REVIEW_ROW_MAPPER);
    }

    @Override
    public List<Review> readAll() throws RepositoryException {
        return jdbcTemplate.query(SQL_FIND_ALL, REVIEW_ROW_MAPPER);
    }

    @Override
    public boolean update(Review review) throws RepositoryException {
        return update(review, getEntityParameters(review));
    }

    private Map<String, Object> getEntityParameters(Review review) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("user_id", review.getUser() != null? review.getUser().getId(): 0);
        parameters.put("content", review.getContent());
        parameters.put("tour_id", review.getTour() != null? review.getTour().getId(): 0);
        return parameters;
    }

    public static class ReviewRowMapper implements RowMapper<Review> {
        @Override
        public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
            Review review = new Review();
            review.setId(rs.getInt("review_id"));
            review.setContent(rs.getString("content"));
            review.setTour(TourDaoEx.TOUR_ROW_MAPPER.mapRow(rs, rowNum));
            review.setUser(UserDaoEx.USER_ROW_MAPPER.mapRow(rs, rowNum));
            return review;
        }
    }
}
