package net.cheltsov.travelagency.repository;

import net.cheltsov.travelagency.entity.Entity;

import java.util.List;

public interface Repository<T extends Entity> {

    /**
     * Add entity to repository. If id of given entity equals zero
     * it will be generated. If not and there is not entity with the
     * same id in repository the entity will be added without any changes.
     * Otherwise RepositoryException will be thrown. If id was generated
     * it will be set to given entity
     *
     * @param entity the entity to add to repository
     * @return the entity witch was added with generated id
     * @throws NullPointerException if the given entity is null
     * @throws RepositoryException if an entity with the same id or
     *          other not allowed duplicated fields
     *          is already exists in repository or is any problem with
     *          Repository
     */

    T create(T entity) throws RepositoryException;

    /**
     * Read entity.
     *
     * @param id the id of entity to find
     * @return the entity which was found or null if there is not the entity
     *          associated with given id
     * @throws RepositoryException if there is any problem with Repository
     */

    T read(int id) throws RepositoryException;

    /**
     * Read all list of entities.
     *
     * @return the list of entities
     * @throws RepositoryException if there is any problem with Repository
     */

    List<T> readAll() throws RepositoryException;

    /**
     * Update entity.
     *
     * @param entity to update
     * @return true if the entity was updated or false
     *          if the repository contained no entity with entity's id.
     * @throws NullPointerException if the given entity is null
     * @throws RepositoryException if there is any problem with Repository
     */

    boolean update(T entity) throws RepositoryException;
    /**
     * Delete entity.
     *
     * @param id the id of entity to delete
     * @return true if the entity was deleted or false
     *          if the repository contained no entity with given id.
     * @throws RepositoryException if there is any problem with Repository
     *
     */
    boolean delete(int id) throws RepositoryException;
}
