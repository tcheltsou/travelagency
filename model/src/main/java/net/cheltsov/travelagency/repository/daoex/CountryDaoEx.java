package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Country;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("daoExs")
@Repository("countryDao")
public class CountryDaoEx extends AbstractDaoEx<Country> {
    private static final String SQL_FIND_ALL = "SELECT country_id, name AS countryName FROM country";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE country_id = :country_id";
    public final static CountryRowMapper COUNTRY_ROW_MAPPER = new CountryRowMapper();

    @Override
    public Country create(Country country) throws RepositoryException {
        return create(country, getEntityParameters(country));
    }

    @Override
    public Country read(int id) throws RepositoryException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("country_id", id),
                COUNTRY_ROW_MAPPER);
    }

    @Override
    public List<Country> readAll() throws RepositoryException {
        return jdbcTemplate.query(SQL_FIND_ALL, COUNTRY_ROW_MAPPER);
    }

    @Override
    public boolean update(Country country) throws RepositoryException {
        return update(country, getEntityParameters(country));
    }

    private Map<String, Object> getEntityParameters(Country country) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", country.getName());
        return parameters;
    }

    public static class CountryRowMapper implements RowMapper<Country> {
        @Override
        public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
            Country country = new Country();
            country.setId(rs.getInt("country_id"));
            country.setName(rs.getString("countryName"));
            return country;
        }
    }

}
