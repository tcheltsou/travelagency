package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Order;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("orderDao")
public class OrderJpa extends AbstractJpa{
    public OrderJpa() {
        super(Order.class);
    }
}
