package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.entity.Order;
import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import net.cheltsov.travelagency.repository.dao.EntityDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional
public abstract class AbstractJpa<T extends Entity> implements Repository<T> {
    private Class<T> genericType;
    private final String checkQuery;
    private final String tableName;
    private final String tableName_id;

    public AbstractJpa() {
        this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), EntityDao.class);
        String quote = "";
        if (Order.class == genericType || User.class == genericType) {
            quote = "\"";
        }
        tableName = quote + genericType.getSimpleName().toLowerCase() + quote;
        tableName_id = genericType.getSimpleName().toLowerCase() + "_id";
        checkQuery = "SELECT * FROM " + tableName + " WHERE " + tableName_id + " = ";
    }

    public AbstractJpa(Class<T> genericType) {
        this.genericType = genericType;
        String quote = "";
        if (Order.class == genericType || User.class == genericType) {
            quote = "\"";
        }
        tableName = quote + genericType.getSimpleName().toLowerCase() + quote;
        tableName_id = genericType.getSimpleName().toLowerCase() + "_id";
        checkQuery = "SELECT * FROM " + tableName + " WHERE " + tableName_id + " = ";

    }

    @Autowired
    private SessionFactory sessionFactory;

    Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }

    @Override
    public T create(T entity) throws RepositoryException {
        try {
            Session session = getCurrentSession();
            List result = session.createSQLQuery(checkQuery + entity.getId()).list();
            if (!result.isEmpty())
                throw new RepositoryException("The entity with id= " + entity.getId() + " already exists");
            session.save(entity);
        } catch (RuntimeException e) {
            throw new RepositoryException(e);
        }
        return entity;
    }

    @Override
    public T read(int id) throws RepositoryException {
        try {
            return getCurrentSession().get(genericType, id);
        } catch (RuntimeException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<T> readAll() throws RepositoryException {
        return getCurrentSession().createQuery("from " + genericType.getSimpleName()).list();
    }

    @Override
    public boolean update(T entity) throws RepositoryException {
        try {
            Session session = getCurrentSession();
            List result = session.createSQLQuery(checkQuery + entity.getId()).list();
            if (result.isEmpty()) return false;
//            session.get(genericType, entity.getId());
            session.merge(entity);
        }catch (RuntimeException e) {
            throw new RepositoryException(e);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws RepositoryException {
        try {
            Session session = getCurrentSession();
            T entityToDelete = session.get(genericType, id);
            if (entityToDelete == null) {
                return false;
            }
            session.delete(entityToDelete);
            System.out.println("After deleted");
        } catch (RuntimeException e) {
            throw new RepositoryException(e);
        }
        return true;
    }
}
