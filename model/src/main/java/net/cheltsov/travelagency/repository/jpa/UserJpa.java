package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("userDao")
public class UserJpa extends AbstractJpa<User> {
    public UserJpa() {
        super(User.class);
    }
}
