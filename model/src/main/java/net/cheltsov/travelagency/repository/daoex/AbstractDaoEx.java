package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public abstract class AbstractDaoEx <T extends Entity> implements Repository<T> {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    NamedParameterJdbcTemplate jdbcTemplate;
    private KeyHolder keyHolder = new GeneratedKeyHolder();
    private String tableName;
    private String tableName_id;

    public AbstractDaoEx() {
        tableName = GenericTypeResolver.resolveTypeArgument(getClass(), MethodHandles.lookup().lookupClass()).getSimpleName().toLowerCase();
        tableName_id = tableName + "_id";
        if ("order".equals(tableName) || "user".equals(tableName)) {
            tableName = "\"" + tableName + "\"";
        }
    }

    public AbstractDaoEx(String tableName) {
        this.tableName = tableName;
        tableName_id = tableName + "_id";
        if ("order".equals(tableName) || "user".equals(tableName)) {
            this.tableName = "\"" + tableName + "\"";
        }
    }

    @Autowired
    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    T create(T entity, Map<String, Object> parameters) throws RepositoryException {
        if (entity.getId() != 0) {
            parameters.put(tableName_id, entity.getId());
        } else {
            parameters.remove(tableName_id);
        }

        final String SQL_INSERT = "INSERT INTO " + tableName + " ("
                + concatParameters(parameters.keySet(), x -> x += ", ")
                + ") VALUES ("
                + concatParameters(parameters.keySet(), x -> x = ":" + x + ", ")
                + ")";

        while (true){
            try {
                jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource(parameters), keyHolder, new String[]{tableName_id});
                if (entity.getId() == 0) {
                    entity.setId((Integer) keyHolder.getKeys().get(tableName_id));
                }
                return entity;
            } catch (DuplicateKeyException e) {
                if (entity.getId() == 0) {
                    logger.warn("Generated key already exists", e);
                } else {
                    throw new RepositoryException("The entity with id=" + entity.getId() + " already exists" , e);
                }
            } catch (DataAccessException e) {
                throw new RepositoryException(e);
            }
        }
    }

    private String concatParameters(Set<String> strings, Function<String, String> mapper) {
        return strings.stream()
                .map(mapper)
                .reduce(String::concat)
                .map(x -> x.substring(0, x.length() - 2))
                .get();
    }

    boolean update(T entity,  Map<String, Object> parameters) throws RepositoryException {
        parameters.remove(tableName_id);
        final String SQL_UPDATE = "UPDATE " + tableName + " SET "
                + concatParameters(parameters.keySet(), x -> x = x + " = :" + x + ", ")
                + " WHERE " + tableName_id + " = :" + tableName_id;
        parameters.put(tableName_id, entity.getId());
        return jdbcTemplate.update(SQL_UPDATE, parameters) == 1;
    }

    @Override
    public boolean delete(int id) throws RepositoryException {
        Map<String, Integer> idParameter = new HashMap<>();
        idParameter.put(tableName_id, id);
        final String SQL_DELETE = "DELETE FROM " + tableName + " WHERE " + tableName_id + " = :" + tableName_id;
        return jdbcTemplate.update(SQL_DELETE, idParameter) == 1;
    }
}
