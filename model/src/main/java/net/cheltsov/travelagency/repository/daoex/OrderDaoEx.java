package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Order;
import net.cheltsov.travelagency.entity.Tour;
import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Profile("daoExs")
@Repository("orderDao")
public class OrderDaoEx extends AbstractDaoEx<Order> {
    static final String SQL_FIND_ALL = "SELECT o.order_id, o.quantity, o.user_id, u.password, u.login, "
            + " o.tour_id, t.date, t.duration, t.tour_type_id, t.description, t.cost, t.photo, "
            + " t.country_id, c.name AS countryName, "
            + " t.hotel_id, h.name AS hotelName, h.phone, h.stars "
            + " FROM \"order\" o "
            + " JOIN \"user\" u ON o.user_id = u.user_id "
            + " JOIN (tour t "
            + "     JOIN country c ON t.country_id = c.country_id "
            + "     JOIN hotel h ON t.hotel_id = h.hotel_id) "
            + " ON o.tour_id = t.tour_id";
    private static String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE o.order_id = :order_id";
    public static final OrderRowMapper ORDER_ROW_MAPPER = new OrderRowMapper();

    @Override
    public Order create(Order order) throws RepositoryException {
        return create(order, getEntityParameters(order));
    }

    @Override
    public Order read(int id) throws RepositoryException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("order_id", id),
                ORDER_ROW_MAPPER);
    }

    @Override
    public List<Order> readAll() throws RepositoryException {
        return jdbcTemplate.query(SQL_FIND_ALL, ORDER_ROW_MAPPER);
    }

    @Override
    public boolean update(Order order) throws RepositoryException {
        return update(order, getEntityParameters(order));
    }

    private Map<String, Object> getEntityParameters(Order order) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("user_id", order.getUser() != null? order.getUser().getId(): 0);
        parameters.put("tour_id", order.getTour() != null? order.getTour().getId(): 0);
        parameters.put("quantity", order.getQuantity());
        return parameters;
    }

    public static class OrderRowMapper implements RowMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
            Order order = new Order();
            order.setId(rs.getInt("order_id"));
            order.setQuantity(rs.getInt("quantity"));
            order.setTour(TourDaoEx.TOUR_ROW_MAPPER.mapRow(rs, rowNum));
            order.setUser(UserDaoEx.USER_ROW_MAPPER.mapRow(rs, rowNum));
            return order;
        }
    }
}
