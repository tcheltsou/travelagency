package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Hotel;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("hotelDao")
public class HotelJpa extends AbstractJpa<Hotel> {
    public HotelJpa() {
        super(Hotel.class);
    }
}
