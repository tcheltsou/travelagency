package net.cheltsov.travelagency.repository.collection;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The type Repository.
 *
 * @param <T> the type parameter
 */
@Deprecated
public class CollectionRepository<T extends Entity> implements Repository<T> {

    private AtomicInteger idGenerator = new AtomicInteger();
    private ConcurrentMap<Integer, T> entities = new ConcurrentHashMap<>();

    /**
     * Instantiates a new Collection repository.
     */
    public CollectionRepository() {
    }

    /**
     * Instantiates a new Collection repository.
     *
     * @param genericType the generic type
     */
    public CollectionRepository(Class<T> genericType) {
    }

    /**
     * Sets entities.
     *
     * @param entities the entities
     */
    public void setEntities(List<T> entities) {
        entities.forEach(this::create);
    }

    @Override
    public T create(T entity) {
        if (entity.getId() != 0) {
            if (entities.putIfAbsent(entity.getId(), entity) != null) {
                throw new RepositoryException("Entity with id="
                        + entity.getId() + "exists");
            }
        } else {
            while (true) {
                entity.setId(idGenerator.incrementAndGet());
                if (entities.putIfAbsent(entity.getId(), entity) == null) {
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public T read(int id) {
        return entities.get(id);
    }

    @Override
    public List<T> readAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public boolean update(T entity) {
        return entities.replace(entity.getId(), entity) != null;
    }

    @Override
    public boolean delete(int id) {
        return entities.remove(id) != null;
    }
}
