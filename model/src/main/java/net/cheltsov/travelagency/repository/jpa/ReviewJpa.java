package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Review;
import net.cheltsov.travelagency.repository.jpa.AbstractJpa;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("reviewDao")
public class ReviewJpa extends AbstractJpa<Review>{
    public ReviewJpa() {
        super(Review.class);
    }
}
