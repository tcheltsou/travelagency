package net.cheltsov.travelagency.repository.dao;

import net.cheltsov.travelagency.entity.*;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.GenericTypeResolver;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

/**
 * The type Entity dao.
 *
 * @param <T> the type parameter
 */
public class EntityDao<T extends Entity> implements Repository<T>, InitializingBean {

    private String quot = "";
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private Class<T> genericType;
    private final String tableName;
    private JdbcTemplate jdbcTemplate;
    private KeyHolder keyHolder = new GeneratedKeyHolder();
    private Map<Class<? extends Entity>, EntityDao> daos;
    private Optional<? extends Entity> stopRecursionEntity = Optional.empty();
    @Autowired
    ListableBeanFactory beanFactory;

    @Override
    public void afterPropertiesSet() {
        daos = new HashMap<>();
        beanFactory.getBeansOfType(EntityDao.class).values()
                .forEach(x -> daos.put((Class)GenericTypeResolver.resolveTypeArgument(x.getClass(), EntityDao.class), x));
    }

    @Profile("daos")
    @org.springframework.stereotype.Repository("countryDao")
    public static class CountryDao extends EntityDao<Country> {}
    @Profile("daos")
    @org.springframework.stereotype.Repository("hotelDao")
    public static class HotelDao extends EntityDao<Hotel> {}
    @Profile("daos")
    @org.springframework.stereotype.Repository("orderDao")
    public static class OrderDao extends EntityDao<Order> {}
    @Profile("daos")
    @org.springframework.stereotype.Repository("reviewDao")
    public static class ReviewDao extends EntityDao<Review> {}
    @Profile("daos")
    @org.springframework.stereotype.Repository("tourDao")
    public static class TourDao extends EntityDao<Tour> {}
    @Profile("daos")
    @org.springframework.stereotype.Repository("userDao")
    public static class UserDao extends EntityDao<User> {}

    /**
     * Instantiates a new Entity dao.
     *
     * @param genericType the generic type
     */
    public EntityDao(Class<T> genericType) {
        this.genericType = genericType;
        tableName = genericType.getSimpleName().toLowerCase();
    }

    /**
     * Instantiates a new Entity dao.
     */
    public EntityDao() {
        this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), EntityDao.class);
        tableName = genericType.getSimpleName().toLowerCase();
    }

    /**
     * Sets daos.
     *
     * @param daos the daos
     */
    public void setDaos(Map<Class<? extends Entity>, EntityDao> daos) {
        this.daos = daos;
    }

    /**
     * Sets jdbc template.
     *
     * @param jdbcTemplate the jdbc template
     */
    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        if (genericType == User.class || genericType == Order.class) {
            quot = "\"";
        }
    }

    @Override
    public T create(T entity) throws RepositoryException {
        Field[] fields = Stream.of(entity.getClass().getDeclaredFields())
                .filter(x -> !Collection.class.isAssignableFrom(x.getType()))
                .peek(x -> x.setAccessible(true)).toArray(Field[]::new);

        Stream<String> colonNamesStream = Stream.of(fields)
                .map(this::FieldToColonNameString);

        Stream<String> valuesStream = Stream.of(fields)
                .map(x -> FieldToValueString(x, entity));

        String colonNames = StreamReducer(colonNamesStream);
        String values = StreamReducer(valuesStream);

        if (entity.getId() != 0) {
            final String SQL_INSERT = "INSERT INTO " + quot + tableName + quot + " (" + tableName + "_id, " + colonNames + ") "
                    + "VALUES (" + entity.getId() + ", " + values + ")";
            try {
                jdbcTemplate.update(SQL_INSERT);
                return entity;
            } catch (DuplicateKeyException e) {
                throw new RepositoryException("The entity with id=" + entity.getId() + " already exists", e);
            } catch (DataAccessException e) {
                throw new RepositoryException(e);
            }
        } else {
            final String SQL_INSERT = "INSERT INTO " + quot + tableName + quot + " (" + colonNames + ") VALUES (" + values + ")";
            do {
                try {
                    jdbcTemplate.update(connection -> connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS), keyHolder);
                    entity.setId((Integer) keyHolder.getKeys().get(tableName + "_id"));
                    return entity;
                } catch (DuplicateKeyException e) {
                    logger.warn("Generated key already exists", e);
                } catch (DataAccessException e) {
                    throw new RepositoryException(e);
                }
            } while (true);
        }
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.SERIALIZABLE)
    public T read(int id) throws RepositoryException {
        final String SQL_SELECT = "SELECT * FROM " + quot + tableName + quot + " WHERE " + tableName + "_id = " + id;
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(SQL_SELECT);
        rowSet.first();
        T entity = createEntityFromRowSet(rowSet);
        return entity;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.SERIALIZABLE)
    public List<T> readAll() throws RepositoryException {
        final String SQL_SELECT_ALL = "SELECT * FROM " + quot + tableName + quot;
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(SQL_SELECT_ALL);
        List<T> entities = new ArrayList<>();
        while (rowSet.next()) {
            entities.add(createEntityFromRowSet(rowSet));
        }
        return entities;
    }

    private T createEntityFromRowSet(SqlRowSet rowSet) {
        T entity;
        try {
            entity = genericType.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
        Field[] fields = entity.getClass().getDeclaredFields();
        T finalEntity = entity;
        entity.setId(rowSet.getInt(tableName + "_id"));
        Stream.of(fields)
                .peek(x -> x.setAccessible(true))
                .forEach(x -> setField(x, finalEntity, rowSet));
        return entity;
    }

    @Override
    public boolean update(T entity) throws RepositoryException {
        Stream<String> stream = Stream.of(entity.getClass().getDeclaredFields())
                .filter(x -> !Collection.class.isAssignableFrom(x.getType()))
                .peek(x -> x.setAccessible(true))
                .filter(x -> {
                    try {
                        return x.get(entity) != null;
                    } catch (IllegalAccessException e) {
                        logger.warn("", e);
                        return false;
                    }
                })
                .map(x -> FieldToColonNameString(x) + " = " + FieldToValueString(x, entity))
                .filter(x -> !x.isEmpty());

        String updateString = StreamReducer(stream);
        final String SQL_UPDATE = "UPDATE " + quot + tableName + quot + " SET " + updateString
                + " WHERE " + tableName + "_id = " + entity.getId();
        return update(SQL_UPDATE);
    }

    @Override
    public boolean delete(int id) throws RepositoryException {
        final String SQL_DELETE = "DELETE FROM " + quot + tableName + quot + " WHERE " + tableName + "_id = " + id;
        return update(SQL_DELETE);
    }

    private boolean update(String query) {
        try {
            return jdbcTemplate.update(query) != 0;
        } catch (EmptyResultDataAccessException e) {
            logger.warn(tableName + " with given id does not exist", e);
            return false;
        } catch (DataAccessException e) {
            throw new RepositoryException(e);
        }
    }

    private String FieldToColonNameString(Field field) {
        if (Entity.class.isAssignableFrom(field.getType())) {
            return field.getName().toLowerCase() + "_id";
        } else if (TourType.class.isAssignableFrom(field.getType())) {
            return "tour_type_id";
        } else {
            return field.getName().toLowerCase();
        }
    }

    private String FieldToValueString(Field field, Entity entity) {
        if (Entity.class.isAssignableFrom(field.getType())) {
            return "'" + ((Entity) ReflectionUtils.getField(field, entity)).getId() + "'";
        } else if (TourType.class.isAssignableFrom(field.getType())) {
            return "'" + ((TourType) ReflectionUtils.getField(field, entity)).ordinal() + "'";
        } else if (new char[0].getClass().equals(field.getType())) {
            return "'" + new String((char[]) ReflectionUtils.getField(field, entity)) + "'";
        } else {
            return "'" + ReflectionUtils.getField(field, entity) + "'";
        }
    }

    private void setField(Field field, Entity entity, SqlRowSet rowSet) {
        if (Entity.class.isAssignableFrom(field.getType())) {
            Class entityType = field.getType();
            if (stopRecursionEntity.isPresent() && stopRecursionEntity.get().getClass() == entityType) {
                ReflectionUtils.setField(field, entity, stopRecursionEntity.get());
                return;
            }
            EntityDao childDao = daos.get(entityType);
            Entity child = childDao.read(rowSet.getInt(field.getType().getSimpleName().toLowerCase() + "_id"));
            ReflectionUtils.setField(field, entity, child);
        } else if (TourType.class.isAssignableFrom(field.getType())) {
            ReflectionUtils.setField(field, entity, TourType.values()[rowSet.getInt("tour_type_id")]);
        } else if (new char[0].getClass().equals(field.getType())) {
            ReflectionUtils.setField(field, entity, rowSet.getString(field.getName().toLowerCase()).toCharArray());
        } else if (List.class.isAssignableFrom(field.getType())) {
            Class childClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            EntityDao childDao = daos.get(childClass);
            ReflectionUtils.setField(field, entity, childDao.findEntitiesByOtherEntity(entity));
        } else if (LocalDate.class.isAssignableFrom(field.getType())) {
            ReflectionUtils.setField(field, entity, rowSet.getTimestamp(field.getName().toLowerCase()).toLocalDateTime().toLocalDate());
        } else {
            ReflectionUtils.setField(field, entity, rowSet.getObject(field.getName().toLowerCase()));
        }
    }

    public List<T> findEntitiesByOtherEntity(Entity parent) {
        final String SQL_FIND_BY = "SELECT * FROM " + quot + tableName + quot + " WHERE "
                + parent.getClass().getSimpleName().toLowerCase() + "_id = " + parent.getId();
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(SQL_FIND_BY);
        List<T> children = new ArrayList<>();
        stopRecursionEntity = Optional.of(parent);
        while (rowSet.next()) {
            children.add(createEntityFromRowSet(rowSet));
        }
        stopRecursionEntity = Optional.empty();
        return children;
    }

    private static String StreamReducer(Stream<String> stream) {
        return stream
                .map(x -> x = x + ", ")
                .reduce(String::concat)
                .map(x -> x.substring(0, x.length() - 2))
                .get();
    }
}
