package net.cheltsov.travelagency.repository;

import net.cheltsov.travelagency.entity.Entity;

/**
 * The interface Repository factory.
 */
public interface RepositoryFactory {
    /**
     * Gets repository.
     *
     * @param <T>    the type parameter
     * @param tClass the the class of type parameter
     * @return the repository
     */
    <T extends Entity> Repository<T> getRepository(Class<T> tClass);

}
