package net.cheltsov.travelagency.repository.jpa;

import net.cheltsov.travelagency.entity.Tour;
import net.cheltsov.travelagency.repository.jpa.AbstractJpa;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jpa")
@Repository("tourDao")
public class TourJpa extends AbstractJpa<Tour> {
    public TourJpa() {
        super(Tour.class);
    }
}
