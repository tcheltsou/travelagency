package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;

import java.util.Map;

public class DaoExFactory implements RepositoryFactory {
    private Map<Class<? extends Entity>, AbstractDaoEx> repositories;

    public DaoExFactory(Map<Class<? extends Entity>, AbstractDaoEx> repositories) {
        this.repositories = repositories;
    }

    @Override
    public <T extends Entity> Repository<T> getRepository(Class<T> tClass) {
        return repositories.get(tClass);
    }
}
