package net.cheltsov.travelagency.repository.collection;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.RepositoryFactory;

import java.util.Map;

/**
 * The type Repository collection factory.
 */
public class RepositoryCollectionFactory implements RepositoryFactory {
    private Map<Class<? extends Entity>, CollectionRepository> repositories;

    /**
     * Instantiates a new Repository collection factory.
     *
     * @param repositories the repositories
     */
    public RepositoryCollectionFactory(Map<Class<? extends Entity>,
                                        CollectionRepository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public <T extends Entity> CollectionRepository<T> getRepository(Class<T> tClass) {
        return repositories.get(tClass);
    }
}
