package net.cheltsov.travelagency.repository.dao;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;

import java.util.Map;

public class DaoFactory implements RepositoryFactory {
    private Map<Class<? extends Entity>, EntityDao> repositories;

    public DaoFactory(Map<Class<? extends Entity>, EntityDao> repositories) {
        this.repositories = repositories;
    }

    @Override
    public <T extends Entity> EntityDao<T> getRepository(Class<T> tClass) {
        return repositories.get(tClass);
    }
}
