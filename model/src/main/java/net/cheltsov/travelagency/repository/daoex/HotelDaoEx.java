package net.cheltsov.travelagency.repository.daoex;

import net.cheltsov.travelagency.entity.Hotel;
import net.cheltsov.travelagency.repository.RepositoryException;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("daoExs")
@Repository("hotelDao")
public class HotelDaoEx extends AbstractDaoEx<Hotel> {
    private static final String SQL_FIND_ALL = "SELECT h.hotel_id, h.name AS hotelName, h.phone, h.stars, h.country_id,"
            + " c.name AS countryName "
            + "FROM hotel h JOIN country c ON h.country_id = c.country_id";
    private static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " WHERE hotel_id = :hotel_id";
    public static final HotelRowMapper HOTEL_ROW_MAPPER = new HotelRowMapper();

    @Override
    public Hotel create(Hotel hotel) throws RepositoryException {
        return create(hotel, getEntityParameters(hotel));
    }

    @Override
    public Hotel read(int id) throws RepositoryException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID,
                new MapSqlParameterSource("hotel_id", id),
                HOTEL_ROW_MAPPER);
    }

    @Override
    public List<Hotel> readAll() throws RepositoryException {
        return jdbcTemplate.query(SQL_FIND_ALL, HOTEL_ROW_MAPPER);
    }

    @Override
    public boolean update(Hotel hotel) throws RepositoryException {
        return update(hotel, getEntityParameters(hotel));
    }

    private Map<String, Object> getEntityParameters(Hotel hotel) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", hotel.getName());
        parameters.put("phone", hotel.getPhone());
        parameters.put("country_id", hotel.getCountry() != null? hotel.getCountry().getId(): 0);
        parameters.put("stars", hotel.getStars());
        return parameters;
    }

    public static class HotelRowMapper implements RowMapper<Hotel> {
        @Override
        public Hotel mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hotel hotel = new Hotel();
            hotel.setCountry(CountryDaoEx.COUNTRY_ROW_MAPPER.mapRow(rs, rowNum));
            hotel.setId(rs.getInt("hotel_id"));
            hotel.setName(rs.getString("hotelName"));
            hotel.setPhone(rs.getString("phone"));
            hotel.setStars(rs.getInt("stars"));
            return hotel;
        }
    }

}
