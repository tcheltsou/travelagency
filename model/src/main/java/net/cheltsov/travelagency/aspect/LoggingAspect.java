package net.cheltsov.travelagency.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.lang.invoke.MethodHandles;

@Aspect
@Component
public class LoggingAspect {
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Pointcut("execution(public * net.cheltsov.travelagency.*.*.*.*(..))")
    private void repositoryLogging() {}

    @Before("repositoryLogging()")
    public void logBefore(JoinPoint joinPoint) {
        logger.info("BEFORE: " + joinPoint.getTarget().getClass().getSimpleName() + " "
                + joinPoint.getSignature().getName());
    }

    @After("repositoryLogging()")
    public void logAfter(JoinPoint joinPoint) {
        logger.info("AFTER: " + joinPoint.getTarget().getClass().getSimpleName() + " "
                + joinPoint.getSignature().getName());
    }

    @AfterReturning(pointcut = "repositoryLogging()" , returning = "returnValue")
    public void logAfterReturning(Object returnValue) {
        logger.info("RETURN VALUE: " + returnValue);
    }

    @Around("repositoryLogging()")
    public Object timeTracker(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object value = joinPoint.proceed();
        stopWatch.stop();
        logger.info("TIME of " + joinPoint.getSignature().getName() + " method: " + stopWatch.getTotalTimeMillis() + "ms");
        return value;
    }

    @AfterThrowing(pointcut = "repositoryLogging()", throwing = "e")
    public void logAfterThrowing(Throwable e) {
        logger.warn("Thrown: " + e);
    }

}
