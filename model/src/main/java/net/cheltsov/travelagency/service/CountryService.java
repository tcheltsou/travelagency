package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Country;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The type Country service.
 */
@org.springframework.stereotype.Service
public class CountryService extends Service<Country> {

    public CountryService(RepositoryFactory factory) {
        super(factory);
    }

    @Autowired
    public CountryService(Repository<Country> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic
}
