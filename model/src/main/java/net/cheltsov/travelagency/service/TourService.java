package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Tour;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The type Tour service.
 */
@org.springframework.stereotype.Service
public class TourService extends Service<Tour> {

    @Autowired
    public TourService(Repository<Tour> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic
}
