package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;

import java.util.List;

/**
 * The type Service.
 *
 * @param <T> the type parameter
 */
public abstract class Service<T extends Entity> {

    private Repository<T> entities;

    /**
     * Instantiates a new Service.
     *
     * @param factory the RepositoryFactory
     */
    public Service(RepositoryFactory factory) {
        this.entities = factory.getRepository((Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), Service.class));
    }

    public Service(Repository<T> entities) {
        this.entities = entities;
    }

    /**
     * Create entity.
     *
     * @param entity the entity to add to repository
     * @return the entity witch was added
     * @throws NullPointerException if the given entity is null
     * @throws RepositoryException if an entity with the same id
     *          is already exists in repository
     */

    protected T create(T entity) throws RepositoryException {
        return entities.create(entity);
    }

    /**
     * Read t.
     *
     * @param id the id of entity to find
     * @return the entity which was found or null if there is not the entity
     *          associated with given id
     */
    public T read(int id) throws RepositoryException {
        return entities.read(id);
    }

    /**
     * Read all list of entities.
     *
     * @return the list of entities
     */
    public List<T> readAll() throws RepositoryException {
        return entities.readAll();
    }

    /**
     * Update entity.
     *
     * @param entity to update
     * @return true if the entity was updated or false
     *          if the repository contained no entity with entity's id.
     * @throws NullPointerException if the given entity is null
     * @throws RepositoryException if there is any problem with Repository
     */
    protected boolean update(T entity) {
        return entities.update(entity);
    }

    /**
     * Delete entity.
     *
     * @param id the id of entity to delete
     * @return true if the entity was deleted or false
     *          if the repository contained no entity with given id.
     * @throws RepositoryException if there is any problem with Repository
     *
     */
    public boolean delete(int id) {
        return entities.delete(id);
    }

}
