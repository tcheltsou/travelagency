package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Order;
import net.cheltsov.travelagency.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class OrderService extends Service<Order> {

    @Autowired
    public OrderService(Repository<Order> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic

}
