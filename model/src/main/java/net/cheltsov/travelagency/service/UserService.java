package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The type User service.
 */
@org.springframework.stereotype.Service
public class UserService extends Service<User> {

    @Autowired
    public UserService(Repository<User> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic
}
