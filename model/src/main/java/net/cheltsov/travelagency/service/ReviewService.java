package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Review;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The type Review service.
 */
@org.springframework.stereotype.Service
public class ReviewService extends Service<Review> {

    @Autowired
    public ReviewService(Repository<Review> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic
}
