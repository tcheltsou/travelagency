package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Hotel;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The type Hotel service.
 */
@org.springframework.stereotype.Service
public class HotelService extends Service<Hotel> {

    @Autowired
    public HotelService(Repository<Hotel> entities) {
        super(entities);
    }
    // TODO: 3/20/2018 Waiting for specific logic
}
