INSERT INTO country (country_id, name) VALUES (1, 'Беларусь');
INSERT INTO country (country_id, name) VALUES (2, 'Польша');
INSERT INTO country (country_id, name) VALUES (3, 'Германия');
INSERT INTO country (country_id, name) VALUES (669, 'Country669');

INSERT INTO "user" (user_id, password, login) VALUES (1, '123456', 'Коля');
INSERT INTO "user" (user_id, password, login) VALUES (2, '123456', 'Маня');
INSERT INTO "user" (user_id, password, login) VALUES (3, '123456', 'Лера');
INSERT INTO "user" (user_id, password, login) VALUES (669, '669', 'login669');

INSERT INTO hotel (hotel_id, name, phone, country_id, stars) VALUES (1, 'Hilton', '+375291234567', 1, 2);
INSERT INTO hotel (hotel_id, name, phone, country_id, stars) VALUES (2, 'Mariot', '+48297894561 ', 2, 3);
INSERT INTO hotel (hotel_id, name, phone, country_id, stars) VALUES (3, 'Azimut', '+493517958990', 3, 4);
INSERT INTO hotel (hotel_id, name, phone, country_id, stars) VALUES (669, 'Hotel669', '+669', 1, 669);

INSERT INTO tour (tour_id, date, duration, country_id, tour_type_id, description, cost, photo, hotel_id) VALUES (1, '2018-03-29', 9, 1, 3, 'description1', 199, 'photo1', 1);
INSERT INTO tour (tour_id, date, duration, country_id, tour_type_id, description, cost, photo, hotel_id) VALUES (2, '2018-03-30', 8, 2, 2, 'description2', 99, 'photo2', 2);
INSERT INTO tour (tour_id, date, duration, country_id, tour_type_id, description, cost, photo, hotel_id) VALUES (3, '2018-03-31', 9, 3, 2, 'description3', 158, 'photo3', 3);
INSERT INTO tour (tour_id, date, duration, country_id, tour_type_id, description, cost, photo, hotel_id) VALUES (669, '2066-09-09', 669, 1, 2, 'description669', 669, 'photo669', 3);

INSERT INTO "order" (order_id, user_id, tour_id, quantity) VALUES (1, 1, 1, 1);
INSERT INTO "order" (order_id, user_id, tour_id, quantity) VALUES (2, 2, 2, 2);
INSERT INTO "order" (order_id, user_id, tour_id, quantity) VALUES (3, 2, 3, 3);
INSERT INTO "order" (order_id, user_id, tour_id, quantity) VALUES (669, 1, 3, 669);

INSERT INTO review (review_id, user_id, content, tour_id) VALUES (1, 1, 'Content1', 1);
INSERT INTO review (review_id, user_id, content, tour_id) VALUES (2, 1, 'Content2', 2);
INSERT INTO review (review_id, user_id, content, tour_id) VALUES (3, 3, 'Content3', 3);
INSERT INTO review (review_id, user_id, content, tour_id) VALUES (669, 1, 'Content669', 1);

