package net.cheltsov.travelagency;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ImportResource("classpath:/collection.xml")
@ComponentScan(basePackages = "net.cheltsov.travelagency")
@PropertySource("jpa-test.properties")
@EnableTransactionManagement
public class AppConfigTest {
    //    @Autowired
    private static Environment environment;

    @Autowired
    public void setEnvironment(Environment environment) {
        AppConfigTest.environment = environment;
        System.out.println(environment);
    }


    @Bean
    @Profile("daoExs")
    public static NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    @Profile("daos")
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Profile({"daos", "daoExs"})
    public PlatformTransactionManager platformTransactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    @Profile("jpa")
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }

    @Bean
    @Profile("jpa")
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan("net.cheltsov.travelagency.entity");
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    private Properties hibernateProperties() {
        return new Properties() {
            {
                System.out.println(environment);
                setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
                setProperty("hibernate.dialect", environment.getProperty("hibernate.dialect"));
                setProperty("hibernate.globally_quoted_identifiers", "true");
                setProperty("hibernate.cache.region.factory_class", environment.getProperty("hibernate.cache.region.factory_class"));
                setProperty("hibernate.cache.use_second_level_cache", environment.getProperty("hibernate.cache.use_second_level_cache"));
                setProperty("hibernate.cache.use_query_cache", environment.getProperty("hibernate.cache.use_query_cache"));
                setProperty("hibernate.globally_quoted_identifiers", "false");
            }
        };
    }

    @Configuration
    @Profile("externalDataBase")
    @PropertySource("classpath:/jdbc-test.properties")
    public static class ConfigExternalDataBase {

        @Value("classpath:/refresh-externalDB-db.sql")
        private Resource refreshScript;
        @Value("classpath:/create-db.sql")
        private Resource createScript;
        @Value("classpath:/insert-data.sql")
        private Resource insertScript;

        @Bean(destroyMethod = "close")
        public BasicDataSource dataSource() {
            BasicDataSource dataSource = new BasicDataSource();
            dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
            dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
            dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
            dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
            ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
            populator.addScript(refreshScript);
            populator.addScript(createScript);
            populator.addScript(insertScript);
            DatabasePopulatorUtils.execute(populator, dataSource);
            return dataSource;
        }
    }

    @Configuration
    @Profile("embeddedDataBase")
    public static class ConfigEmbeddedDataBase {
        @Bean
        public DataSource dataSource() {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("create-db.sql")
                    .addScript("insert-data.sql")
                    .build();
        }
    }

}

