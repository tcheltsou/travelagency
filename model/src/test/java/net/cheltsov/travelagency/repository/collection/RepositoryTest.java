package net.cheltsov.travelagency.repository.collection;

import net.cheltsov.travelagency.entity.Country;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import net.cheltsov.travelagency.repository.collection.CollectionRepository;
import net.cheltsov.travelagency.repository.collection.Countries;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class RepositoryTest {
    private Repository<Country> countries;

    @Before
    public void init() throws RepositoryException {
        countries = new CollectionRepository<>();
        countries.create(Countries.BELARUS);
        countries.create(Countries.USA);
    }

    @Test
    public void testCreate() throws RepositoryException {
        Country expectedCountry = new Country("Germany");
        countries.create(expectedCountry);
        Country actualCountry = countries.read(3);
        assertEquals(expectedCountry, actualCountry);
    }

    @Test(expected = RepositoryException.class)
    public void testCreateException() throws RepositoryException {
        Country country = new Country("Germany");
        country.setId(1);
        countries.create(country);
    }

    @Test
    public void testReadExist() throws RepositoryException {
        Country actualCountry = countries.read(1);
        Country expectedCountry = Countries.BELARUS;
        assertEquals(expectedCountry, actualCountry);
    }

    @Test
    public void testReadNotExistNull() throws RepositoryException {
        Country country = countries.read(99);
        assertNull(country);
    }

    @Test
    public void testReadAll() throws RepositoryException {
        List<Country> expectedList = new ArrayList<>();
        expectedList.add(Countries.BELARUS);
        expectedList.add(Countries.USA);
        expectedList.sort(Comparator.comparing(Country::getName));
        List<Country> actualList = countries.readAll();
        actualList.sort(Comparator.comparing(Country::getName));
        assertEquals(expectedList, actualList);
    }

    @Test
    public void testUpdate() throws RepositoryException {
        Country expectedCountry = new Country("Canada");
        expectedCountry.setId(1);
        assertTrue(countries.update(expectedCountry));
        Country actualCountry = countries.read(1);
        assertEquals(expectedCountry, actualCountry);
    }

    @Test
    public void updateNotExist() throws RepositoryException {
        Country country = new Country("Canada");
        country.setId(99);
        assertFalse(countries.update(country));
        Country actualCountry = countries.read(99);
        assertNull(actualCountry);
    }

    @Test
    public void testDelete() throws RepositoryException {
        assertTrue(countries.delete(1));
        Country country = countries.read(1);
        assertNull(country);
    }

}