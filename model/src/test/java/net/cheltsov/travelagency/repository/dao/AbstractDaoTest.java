package net.cheltsov.travelagency.repository.dao;

import net.cheltsov.travelagency.AppConfigTest;
import net.cheltsov.travelagency.entity.Entity;
import net.cheltsov.travelagency.entity.User;
import net.cheltsov.travelagency.repository.Repository;
import net.cheltsov.travelagency.repository.RepositoryException;
import net.cheltsov.travelagency.repository.collection.CollectionRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

public abstract class AbstractDaoTest<T extends Entity> {
    private Class<T> genericType;
    private final String tableName;
    private AnnotationConfigApplicationContext ctx;
    private Repository<T> dao;
    private CollectionRepository<T> repository;
    private int extraIdRepo = 99;
    private int extraIdDao = 669;
    private int sameEntityId = 2;
    private int excludeEntityId = 1;

    public AbstractDaoTest() {
        this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractDaoTest.class);
        tableName = genericType.getSimpleName().toLowerCase();
    }

    @Before
    public void init() {
        ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("embeddedDataBase", "jpa");
        ctx.register(AppConfigTest.class);
        ctx.refresh();
        dao = (Repository<T>) ctx.getBean(tableName + "Dao");
        repository = (CollectionRepository<T>) ctx.getBean(tableName + "Repository");
    }

    @After
    public void destroy() {
        ctx.close();
    }

    @Test
    public void createWithGivenId() {
        T entityExpected = repository.read(extraIdRepo);
        T entityActual = dao.create(entityExpected);
        dao.read(extraIdRepo);
        assertEquals(entityExpected, entityActual);
        // TODO: 4/6/2018 проверить есть ли в базе
    }

    @Test
    public void createWithoutId() {
        T entityExpected = repository.read(extraIdRepo);
        entityExpected.setId(0);
        T entityActual = dao.create(entityExpected);
        assertEquals(entityExpected, entityActual);
    }

    @Test(expected = RepositoryException.class)
    public void createIdExistsException() {
        T entityExpected = repository.read(extraIdRepo);
        entityExpected.setId(1);
        dao.create(entityExpected);
    }

    @Test
    public void read() {
        T entityExpected = repository.read(sameEntityId);
        T entityActual = dao.read(sameEntityId);
        assertEquals(entityExpected, entityActual);
    }

    @Test
    public void readNotExistNull() {
        T entityExpected = repository.read(9);
        assertNull(entityExpected);
    }

    @Test
    public void readAll() {
        List<T> entitiesExpected = repository.readAll();
        entitiesExpected.removeIf(x -> x.getId() == extraIdRepo || x.getId() == excludeEntityId);
        List<T> entitiesActual = dao.readAll();
        entitiesActual.removeIf(x -> x.getId() == extraIdDao || x.getId() == excludeEntityId);
        assertEquals(entitiesExpected, entitiesActual);
    }

    @Test
    public void update() {
        T entityExpected = repository.read(extraIdRepo);
        entityExpected.setId(extraIdDao);
        boolean ifUpdated = dao.update(entityExpected);
        assertTrue(ifUpdated);
//        T entityActual = dao.read(extraIdDao);
//        assertEquals(entityExpected, entityActual);
    }

    @Test
    public void updateNotExist() {
        T entityExpected = repository.read(extraIdRepo);
        boolean ifUpdated = dao.update(entityExpected);
        assertFalse(ifUpdated);
    }

    @Test
    public void delete() {
//        if (dao.read(extraIdDao) == null) {
//            fail();
//        };
        boolean ifDelete = dao.delete(extraIdDao);
        assertTrue(ifDelete);
        assertNull(repository.read(extraIdDao));
    }

    @Test
    public void deleteNotExist() {
        boolean ifDeleted = dao.delete(42);
        assertFalse(ifDeleted);
    }


}