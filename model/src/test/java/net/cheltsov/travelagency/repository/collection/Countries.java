package net.cheltsov.travelagency.repository.collection;

import net.cheltsov.travelagency.entity.Country;

public class Countries {
    public static final Country BELARUS = new Country("Belarus");
    public static final Country USA = new Country("USA");

    private Countries() {
    }

    static {
        BELARUS.setId(1);
        USA.setId(2);
    }
}
