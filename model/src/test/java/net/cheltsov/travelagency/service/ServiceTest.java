package net.cheltsov.travelagency.service;

import net.cheltsov.travelagency.entity.Country;
import net.cheltsov.travelagency.repository.collection.Countries;
import net.cheltsov.travelagency.repository.collection.CollectionRepository;
import net.cheltsov.travelagency.repository.Repository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ServiceTest {

    private static Repository<Country> countries = mock(CollectionRepository.class);
    private static CountryService countryService = new CountryService(countries);

    @Before
    public void init() {
        reset(countries);
    }

    @Test
    public void testCreate() throws Exception {
        when(countries.create(Countries.BELARUS)).thenReturn(Countries.BELARUS);
        assertEquals(Countries.BELARUS, countryService.create(Countries.BELARUS));
    }

    @Test
    public void testRead() {
        when(countries.read(1)).thenReturn(Countries.BELARUS);
        assertEquals(Countries.BELARUS, countryService.read(1));
    }

    @Test
    public void testReadAll() {
        List<Country> object = new ArrayList<>();
        when(countries.readAll()).thenReturn(object);
        assertEquals(object, countryService.readAll());
    }

    // TODO: 3/28/2018 переделать

//    @Test
//    public void testUpdate() throws Exception {
//        when(countries.update(Countries.BELARUS)).thenReturn(Countries.BELARUS);
//        assertEquals(Countries.BELARUS, countryService.update(Countries.BELARUS));
//    }
//
//    @Test
//    public void testDelete() {
//        when(countries.delete(1)).thenReturn(Countries.BELARUS);
//        assertEquals(Countries.BELARUS, countryService.delete(1));
//    }

}